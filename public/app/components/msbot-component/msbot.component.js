(function () {
    'use strict';
    
    angular.module('travelersBotApp').component('msbot', {
        templateUrl: '../public/app/components/msbot-component/templates/msbot.html',
        controller: MsBotController,
        controllerAs: "vm",
        bindings: {
            luisKey: '=',
        }
    });

    MsBotController.$inject = ['msbot.dataservice'];

    function MsBotController(service) {
        var vm = this;

        //properties
        vm.botConversation = {};
        vm.luisReturnData = {};
        vm.utterence = null;

        //functions
        vm.sendUtterence = sendUtterence;

        vm.$onInit = function() {
        };

        function sendUtterence() {
            service.getIntent(vm.utterence).then(function(data){
                vm.luisReturnData = data;
            });
        }
    }
})();