var express = require("express");
var router = express.Router();
var app = express();

//Bot Route Definitions
var msbot = require('./bots/microsoft/officequerybot')

app.use('/', express.static(__dirname + '/'));
app.use('/', express.static(__dirname + '/dist'));
app.use('/', express.static(__dirname + '/public/app'));
app.use('/', express.static(__dirname + '/public'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));
app.use(msbot);

app.listen(3000);
console.log('listening on port 3000');