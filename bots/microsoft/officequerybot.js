var express = require("express");
var router = express.Router();
var builder = require('botbuilder');
var data = require('../data/answers');

var connector = new builder.ChatConnector({
    appId: '81782913-3463-4201-ac1a-f3b31e920db3',
    appPassword: 'W1YkotCdfb5qKynAiiJQB8e'
});

var LuisModelUrl = 'https://eastus2.api.cognitive.microsoft.com/luis/v2.0/apps/6d9dcee7-b83a-439f-8069-ade0eeefea13?subscription-key=580fe73f9dcb4ee8b3b9ca642e5d1eab';
var bot = new builder.UniversalBot(connector);
router.post('/api/messages', connector.listen());

var recognizer = new builder.LuisRecognizer(LuisModelUrl);
var intents = new builder.IntentDialog({ recognizers: [recognizer] });
bot.dialog('/', intents);

intents.onDefault([
    function(session, args, next){
        session.send(data[args.intent]);
	}
]);

module.exports = router;