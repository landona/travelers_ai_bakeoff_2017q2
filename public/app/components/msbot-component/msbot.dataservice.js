(function() {
    'use strict'

    angular.module('travelersBotApp')
        .factory('msbot.dataservice', ['$http', 'constants', MsBotDataService]);

    function MsBotDataService($http, constants) {
        return {
            testService: testService,
            getIntent: getIntent
        };

        function testService(data) {
            var url = 'http://localhost:3000/api/messages'
            return $http.post(url, data, {
                headers: {'Authorization': 'Bearer oqRyMD2cOvM.cwA.P8o.D_-PmnuxIVmzlUQ88ROQPpgRtSCST7en4HCVvF4p78A'}
            }).then(responseData)
              .catch(serviceError);
        }

        function getIntent(utterance) {
            var serviceUrl = constants.luisConfig.url;
            var key = constants.luisConfig.key;
            var url = serviceUrl + '?subscription-key=' + key +'&verbose=true&timezoneOffset=0&q=' + utterance;

            return $http.get(url)
                .then(responseData)
                .catch(serviceError);
        }

        function responseData(response) {
            return response.data;
        }

        function serviceError(error) {
            console.log(error.data);
        }
    }
})();