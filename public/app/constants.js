(function() {
    'use strict';

    angular.module('travelersBotApp').constant('constants', {
	    luisConfig: {
            url: 'https://eastus2.api.cognitive.microsoft.com/luis/v2.0/apps/6d9dcee7-b83a-439f-8069-ade0eeefea13',
            key: '580fe73f9dcb4ee8b3b9ca642e5d1eab'
        }
    });
})();