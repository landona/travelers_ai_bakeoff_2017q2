module.exports = function () {
    var config = {
        vendorFiles: ['./node_modules/angular/angular.min.js',
            './node_modules/botframework-directlinejs/directLine.js'
        ]
    };

    return {
        config: config
    };
};