var gulpConfig = require('./gulp.config.js')();
var gulp = require('gulp');
var sass = require('gulp-sass');
var angularFilesort = require('gulp-angular-filesort');
var inject = require('gulp-inject');

var config = gulpConfig.config;

gulp.task("default", ['inject-vendor', 'compile-sass'], compile);
gulp.task("inject-vendor", injectVendor);
gulp.task("compile-sass", compileSass);

function compile() {
    var sources = gulp.src('./public/app/**/*.js')
                    .pipe(angularFilesort());

    return injectJs(sources, './public/views/index.html', './public/views');
}

function compileSass() {
    return gulp.src('./public/app/styles/global.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest('./dist/css'));
}

function injectVendor() {
    var sources = gulp.src(config.vendorFiles);
    return injectJs(sources, './public/views/*.html', './public/views', 'lib');
}

function injectJs(sources, target, dest, name) {
    var transform = function (filePath, file) {
        var scriptTag = "<script src='../" + filePath + "'></script>";
        return scriptTag;
    };

    return injectTarget(sources, target, dest, transform, name);
}

function injectTarget(sources, target, dest, transform, name) {

    var options = {
        transform: transform,
        addRootSlash: false,
        ignorePath: "dist"
    }

    if (name) {
        options["name"] = name;
    }

    return gulp.src(target)
        .pipe(inject(sources, options))
        .pipe(gulp.dest(dest));
       
}